# pipeline-angular-cli

Docker image for testing Angular-CLI builds on Bitbucket Pipelines.

Based on node:6.11.3-slim with yarn, phantomjs, Chrome and Angular-CLI.

**Ensure your tests are configured to use ChromeHeadless:**
`ng test --browsers ChromeHeadless`
